package week3.domain;

/**
 * This class contains general functions for using in other classes
 */

public class General {

    public static void wrongCommandMessage() {
        System.out.println("Wrong command! Enter again!");
    }

    public static void newPage() {
        for (int i = 0; i < 10; i++) System.out.println("\n");
    }

    public static void invalidPasswordMessage(){
        System.out.println("Password should contain uppercase and lowercase letters and digits, and its " +
                "length must have at least 10 symbols");
    }

}
