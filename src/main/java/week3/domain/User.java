package week3.domain;

/**
 * This class contains information about user.
 */

public class User {

    private int id;                 //id of User in database
    private static int id_gen = 0;  //generation of id
    private String name;            //name of User
    private String surname;         //surname of User
    private String username;        //username of User
    private Password password;      //object of Password, which User can sign in

    public User(String name, String surname, String username){
        setName(name);
        setSurname(surname);
        setUsername(username);
    }

    public User(String name, String surname, String username, Password password) {
        setId(id_gen);
        setName(name);
        setSurname(surname);
        setUsername(username);
        setPassword(password);
    }

    public User(int id, String name, String surname, String username, Password password) {
        setId(id);
        setName(name);
        setSurname(surname);
        setUsername(username);
        setPassword(password);
    }

    private void setId(int id){
        if(id_gen <= id) {
            id_gen = id+1;
        }
        this.id = id;
    }

    int getId(){
        return id;
    }

    public void setName(String name){
        this.name = name;
    }
    public String getName(){
        return name;
    }

    public void setSurname(String surname){
        this.surname = surname;
    }
    public String getSurname(){
        return surname;
    }

    public void setUsername(String username){
        this.username = username;
    }
    public String getUsername(){
        return username;
    }

    public void setPassword(Password password){
        this.password = password;
    }
    public Password getPassword(){
        return password;
    }

    @Override
    public String toString() {
        return String.valueOf(id)+' '+name+' '+surname+' '+username+' '+password.getPasswordStr();
    }
}
