package week3.domain;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

/**
 * This class is for games
 * User can choose which game he/she wants to play
 */

public class Game {

    private Scanner sc;                 //Scanner for reading data from the user
    private Random rand;                //object of Random, for creating random number

    private Player player;              //user who plays game
    private int score;                  //player's score

    private ArrayList<Player> leaders;  //a field which stores list of players from database
    private String leaderboard;         //leaderboard
    private File database;              //File through which we receive data

    Game(User player) throws FileNotFoundException {
        setPlayer(player);
        sc = new Scanner(System.in);
        rand = new Random();
        leaders = new ArrayList<>();
        leaderboard = "C:\\Users\\Az1zbek\\IdeaProjects\\Project week 3" +
                        "\\src\\main\\java\\week3\\domain\\leaderboard.txt";
        database = new File(leaderboard);
        fillLeaders();
    }

    public void chooseGame() throws IOException {
        while (true) {
            System.out.println("Hello, " + player.getPlayername() + "!");
            System.out.println("What game do you want to play?");
            System.out.println("1. Guess the number.");
            System.out.println("2. Unscramble word");
            System.out.println("3. <-Go back.");
            System.out.println("4. Check my total score.");
            System.out.println("0. Leaderboard.");
            int choice = sc.nextInt();
            if (choice == 1) {
                General.newPage();
                guessTheNumber();
            }
            else if(choice == 2){
                General.newPage();
                unscrambleWord();
            }
            else if (choice == 3) {
                saveLeaderboard();
                General.newPage();
                break;
            }
            else if(choice==4){
                System.out.println(player.getPlayername()+", your total score is "+player.getScore());
            }
            else if(choice == 0){
                General.newPage();
                leaderboard();
            }
            else General.wrongCommandMessage();
        }
    }

    private void guessTheNumber() {
        int guess;
        int bound = 11;
        int answer, chances = 1;
        System.out.println("There is the mistery number which generated randomly from 0 to 10. You must to guess it.");
        System.out.println("Choose the difficulty:");
        System.out.println("1. Easy (0-10).");
        System.out.println("2. Medium (0-20).");
        System.out.println("3. Hard (0-50).");
        System.out.println("4. Very hard (0-100).");
        System.out.println("5. Are you crazy?! (0-500).");
        System.out.println("6. Impossible (0-1000000000).");
        int choice = sc.nextInt();
        switch (choice) {
            case 1:
                bound = 11;
                score+=10;
                break;
            case 2:
                bound = 21;
                score+=20;
                break;
            case 3:
                bound = 51;
                score+=30;
                break;
            case 4:
                bound = 101;
                score+=50;
                break;
            case 5:
                bound = 501;
                score+=100;
                break;
            case 6:
                bound = 1000000001;
                score+=1000;
                break;
            default:
                General.wrongCommandMessage();
        }
        guess = rand.nextInt(bound);
        System.out.print("Guess the mistery number: ");
        answer = sc.nextInt();
        while (answer != guess) {
            System.out.print("Not right. ");
            if (answer > guess) {
                System.out.println("Mistery number is less than " + answer + ". Try again: ");
                answer = sc.nextInt();
                chances++;
            } else if (answer < guess) {
                System.out.println("Mistery number is greater than " + answer + ". Try again: ");
                answer = sc.nextInt();
                chances++;
            }
        }
        score = (score-chances>0) ? score-chances :  0;
        System.out.println("Congratulations, "+player.getPlayername()
                +"! You've guessed the mistery number in " + chances + " chances! You gathered points: "+score+".");
        player.setScore(player.getScore()+score);
        while (true) {
            System.out.println("1. <-Go back.");
            choice = sc.nextInt();
            if (choice != 1) General.wrongCommandMessage();
            else {
                General.newPage();
                break;
            }
        }
    }

    private void unscrambleWord(){
        String ans;
        int chances = 1;
        String[] words = {"rgenerine", "bteadsaa", "rmaprgo", "faeroswt", "icdgon", "pia"};
        int wordPos = rand.nextInt(words.length);
        String someWord = words[wordPos];
        System.out.println("Unscramble the word, which associated with IT: "+someWord);
        score = 100;
        while (true){
            System.out.print("Write your answer: ");
            ans = sc.next();
            if(wordPos == 0 && ans.equals("engineer")) break;
            else if(wordPos == 1 && ans.equals("database")) break;
            else if(wordPos == 2 && ans.equals("program")) break;
            else if(wordPos == 3 && ans.equals("software")) break;
            else if(wordPos == 4 && ans.equals("coding")) break;
            else if(wordPos == 5 && ans.equals("api")) break;
            else chances++;
            System.out.println("Not right. Try again.");
        }
        score = (score-2*chances>0) ? score-2*chances :  0;
        System.out.println("Congratulations, "+player.getPlayername()
                +"! You've unscrambled the word in " + chances + " chances! You gathered points: "+score+".");
        player.setScore(player.getScore()+score);
        while (true) {
            System.out.println("1. <-Go back.");
            int choice = sc.nextInt();
            if (choice != 1) General.wrongCommandMessage();
            else{
                General.newPage();
                break;
            }
        }
    }

    private void fillLeaders() throws FileNotFoundException {
        boolean isExist = false;
        Scanner dbScanner = new Scanner(database);
        while (dbScanner.hasNext()) {
            addLeader(new Player(dbScanner.next(), dbScanner.nextInt()));
        }

        for (Player leader : leaders) {
            if(player.getPlayername().equals(leader.getPlayername())){
                isExist = true;
                player = leader;
                break;
            }
        }

        if(!isExist){
            addLeader(player);
        }
    }

    private void leaderboard(){
        while (true) {
            System.out.println("Leaderboard");
            for (Player leader : leaders) {
                System.out.println(leader.getPlayername() + " " + leader.getScore());
            }
            System.out.println("1. <-Go back.");
            if(sc.nextInt()!=1)General.wrongCommandMessage();
            else break;
        }
    }

    private void saveLeaderboard() throws IOException {
        String data = "";
        for (Player leader : leaders) {
            data += leader + "\n";
        }
        Files.write(Paths.get(leaderboard),
                data.getBytes());
    }

    private void setPlayer(User player){
        this.player = new Player(player);
    }

    private void addLeader(Player player){
        leaders.add(player);
    }

}
