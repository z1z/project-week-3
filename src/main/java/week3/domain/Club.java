package week3.domain;

public class Club {
    private String clubName;
    private String participant;

    Club(String clubName, String participant){
        setClubName(clubName);
        setParticipant(participant);
    }

    public void setClubName(String clubName) {
        this.clubName = clubName;
    }

    public String getClubName() {
        return clubName;
    }

    public void setParticipant(String participant) {
        this.participant = participant;
    }

    public String getParticipant() {
        return participant;
    }

    @Override
    public String toString() {
        return clubName+" "+participant;
    }
}
