package week3.domain;

public class Admin extends User {
    private String club;

    Admin(String club, String name, String surname, String username) {
        super(name, surname, username);    // I wanted to write setClub first, but it needs for write super() first
        setClub(club);
    }

    public void setClub(String club) {
        this.club = club;
    }

    public String getClub() {
        return club;
    }

    @Override
    public String toString() {
        return club+" "+getName()+" "+getSurname()+" "+getUsername();
    }
}
