package week3.domain;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * This is class MyApplication
 * It has authentication, sign up, sign in
 * Information of users are stored in database db.txt
 */

public class MyApplication {

    // users - a list of users
    private ArrayList<User> users;  //a field which stores list of users from database
    private Scanner sc;             //Scanner for reading data from the user
    private User signedUser;        //user who signed in the application
    private String dbPath;          //path for database
    private File database;          //File through which we receive data

    private String pathDBadmins;
    private File dbadmins;
    private ArrayList<Admin> admins;

    private String pathDBclubs;
    private File dbclubs;
    private ArrayList<Club> clubs;

    MyApplication() {
        users = new ArrayList<>();
        sc = new Scanner(System.in);
        dbPath = "C:\\Users\\Az1zbek\\IdeaProjects\\Project week 3\\src\\main\\java\\week3\\domain\\db.txt";
        database = new File(dbPath);

        pathDBadmins = "C:\\Users\\Az1zbek\\IdeaProjects\\Project week 3" +
                "\\src\\main\\java\\week3\\domain\\admins.txt";
        dbadmins = new File(pathDBadmins);
        admins = new ArrayList<>();

        pathDBclubs = "C:\\Users\\Az1zbek\\IdeaProjects\\Project week 3" +
                "\\src\\main\\java\\week3\\domain\\clubs&participants.txt";
        dbclubs = new File(pathDBclubs);
        clubs = new ArrayList<>();
    }

    // Where my application takes a start.
    public void start() throws IOException {
        Scanner dbScanner = new Scanner(database);
        // fill userlist from db.txt
        while (dbScanner.hasNext()) {
            addUser(new User(dbScanner.nextInt(), dbScanner.next(), dbScanner.next(),
                    dbScanner.next(), new Password(dbScanner.next())));
        }
        fillAdmins();
        fillClubs();

        while (true) {
            System.out.println("Welcome to my application!");
            System.out.println("Select command:");
            System.out.println("1. Menu");
            System.out.println("2. Exit");
            int choice = sc.nextInt();
            if (choice == 1) {
                General.newPage();
                notSignedMenu();
            } else if (choice == 2) {
                System.out.println("Come back again! We are waiting for you!");
                break;
            } else {
                General.wrongCommandMessage();
            }
        }
        saveUserList();
        saveAdminList();
        saveClubList();
    }

    private void notSignedMenu() throws IOException {
        while (true) {
            if (signedUser == null) {
                System.out.println("You are not signed in.");
                System.out.println("1. Authentication");
                System.out.println("2. <-Go Back");
                int choice = sc.nextInt();
                if (choice == 1) {
                    General.newPage();
                    authentication();
                } else if (choice == 2) {
                    General.newPage();
                    break;
                } else {
                    General.wrongCommandMessage();
                }
            } else {
                menu();
            }
        }
    }

    private void menu() throws IOException {
        while (signedUser != null) {
            System.out.println("1. View my profile.");
            System.out.println("2. Games.");
            System.out.println("3. Club.");
            System.out.println("4. Settings.");
            System.out.println("0. Logout");
            int choice = sc.nextInt();
            if (choice == 1) {
                General.newPage();
                userProfile();
            } else if (choice == 2) {
                General.newPage();
                games();
            } else if (choice == 3) {
                General.newPage();
                club();
            } else if (choice == 4) {
                General.newPage();
                settings();
            } else if (choice == 0) {
                logOff();
                General.newPage();
                break;
            } else General.wrongCommandMessage();
        }
    }

    private void club() {
        while (true) {
            System.out.println("1. My club.");
            System.out.println("2. Create a club.");
            System.out.println("3. Join to club.");
            System.out.println("4. Leave club.");
            System.out.println("5. <-Go back.");
            int choice = sc.nextInt();
            if (choice == 1) {
                if (isAdmin(signedUser.getUsername())) {
                    for (Admin admin : admins) {
                        if (admin.getUsername().equals(signedUser.getUsername())) {
                            System.out.println("You are admin of " + admin.getClub());
                            break;
                        }
                    }
                } else {
                    boolean isParticipant = false;
                    for (Club club : clubs) {
                        if (club.getParticipant().equals(signedUser.getUsername())) {
                            System.out.println("You are participant of " + club.getClubName());
                            isParticipant = true;
                            break;
                        }
                    }
                    if (!isParticipant) System.out.println("You are not a member of any club");
                }
            } else if (choice == 2) {
                General.newPage();
                if (!isAdmin(signedUser.getUsername())) {
                    String clubname;
                    boolean isExisted = false;
                    System.out.print("Enter name of club: ");
                    while (true) {
                        clubname = sc.next();
                        for (Club club : clubs) {
                            if (clubname.equals(club.getClubName())) {
                                System.out.println("A club with the same name already exists.");
                                isExisted = true;
                                break;
                            }
                        }
                        if (!isExisted) break;

                        boolean exit = false;
                        while (true) {
                            System.out.println("1. Try again.");
                            System.out.println("2. <-Go back.");
                            int c = sc.nextInt();
                            if (c == 1) {
                                break;
                            } else if (c == 2) {
                                exit = true;
                                break;
                            } else {
                                General.wrongCommandMessage();
                            }
                        }
                        if (exit) break;
                    }
                    Admin newAdmin = new Admin(clubname, signedUser.getName(),
                            signedUser.getSurname(), signedUser.getUsername());
                    Club newClub = new Club(clubname, newAdmin.getUsername());
                    admins.add(newAdmin);
                    clubs.add(newClub);
                    System.out.println("You've created new club!");
                } else {
                    System.out.println("Sorry, you are already admin.");
                }
            } else if (choice == 3) {
                General.newPage();
                String searchClub;
                boolean isExisted = false;
                boolean inClub = false;
                while (true) {
                    for (Club club : clubs) {
                        if (club.getParticipant().equals(signedUser.getUsername())) {
                            System.out.println("You are already in club.");
                            inClub = true;
                            break;
                        }
                    }
                    if (inClub) break;
                    System.out.print("Enter club which you want to join: ");
                    searchClub = sc.next();
                    for (Admin admin : admins) {
                        if (admin.getClub().equals(searchClub)) {
                            isExisted = true;
                            break;
                        }
                    }
                    if (isExisted) {
                        clubs.add(new Club(searchClub, signedUser.getUsername()));
                        System.out.println("Congratulations! You've joined to " + searchClub);
                        break;
                    }
                    System.out.println("A club with this name doesn't exist.");
                    boolean exit = false;
                    while (true) {
                        System.out.println("1. Try again.");
                        System.out.println("2. <-Go back.");
                        int c = sc.nextInt();
                        if (c == 1) {
                            break;
                        } else if (c == 2) {
                            exit = true;
                            break;
                        } else {
                            General.wrongCommandMessage();
                        }
                    }
                    if (exit) break;
                }
            } else if (choice == 4) {
                boolean deleted = false;

                if (isAdmin(signedUser.getUsername())) {
                    String clubname = "";
                    for (Admin admin : admins) {
                        if (signedUser.getUsername().equals(admin.getUsername())) {
                            System.out.println("You've successfully left from " + admin.getClub());
                            clubname = admin.getClub();
                            admins.remove(admin);
                            break;
                        }
                    }

                    for (int i = 0; i < clubs.size(); i++) {
                        if (clubname.equals(clubs.get(i).getClubName())) {
                            clubs.remove(clubs.get(i));
                            i--;
                        }
                    }
                } else {
                    for (Club club : clubs) {
                        if (signedUser.getUsername().equals(club.getParticipant())) {
                            General.newPage();
                            System.out.println("You've successfully left from " + club.getClubName());
                            clubs.remove(club);
                            deleted = true;
                            break;
                        }
                    }
                }
                if (!deleted) {
                    System.out.println("You are not in club.");
                }
            } else if (choice == 5) {
                General.newPage();
                break;
            } else {
                General.wrongCommandMessage();
            }
        }
    }

    private void userProfile() {
        while (true) {
            System.out.println("Your profile.");
            System.out.println("Name:      " + signedUser.getName() +
                    "\nSurname:   " + signedUser.getSurname() +
                    "\nUsername:  " + signedUser.getUsername());

            System.out.println("1. <-Go back.");
            System.out.println("0. Logout.");
            int choice = sc.nextInt();
            if (choice == 0) {
                logOff();
                General.newPage();
                break;
            } else if (choice == 1) {
                General.newPage();
                break;
            } else General.wrongCommandMessage();
        }
    }

    private void settings() {
        while (true) {
            System.out.println("1. Change password");
            System.out.println("2. <-Go back.");
            int choice = sc.nextInt();
            if (choice == 1) {
                General.newPage();
                changePassword();
            } else if (choice == 2) {
                General.newPage();
                break;
            } else General.wrongCommandMessage();
        }
    }

    private void changePassword() {
        String currentPas;
        String newPas;
        String passVerification;
        System.out.print("Enter your current password: ");
        currentPas = sc.next();
        while (!currentPas.equals(signedUser.getPassword().getPasswordStr())) {
            System.out.print("Invalid password. Try again: ");
            currentPas = sc.next();
        }
        System.out.print("Enter new password: ");
        newPas = sc.next();
        while (!signedUser.getPassword().checkPassword(newPas)) {
            General.invalidPasswordMessage();
            System.out.print("Try again: ");
            newPas = sc.next();
        }
        System.out.print("Re-write you new password: ");
        passVerification = sc.next();
        while (!passVerification.equals(newPas)) {
            System.out.print("They aren't the same. Try again: ");
            passVerification = sc.next();
        }
        signedUser.getPassword().setPasswordStr(newPas);
        General.newPage();
        System.out.println("You have successfully changed your password.");
    }

    private void games() throws IOException {
        Game game = new Game(signedUser);
        game.chooseGame();
    }

    private void authentication() {
        while (true) {
            System.out.println("1. Sign in.");
            System.out.println("2. Don't you have an account? Sign up now!");
            System.out.println("3. <-Go Back.");
            int choice = sc.nextInt();
            if (choice == 1) {
                General.newPage();
                signIn();
                break;
            } else if (choice == 2) {
                General.newPage();
                signUp();
            } else if (choice == 3) {
                General.newPage();
                break;
            } else {
                General.wrongCommandMessage();
            }
        }
    }

    private void signIn() {
        String username;
        String password;
        boolean validUsername;
        boolean validPassword;
        User userInDb = null;

        while (true) {
            validUsername = false;
            validPassword = false;

            System.out.print("Enter your username: ");
            username = sc.next();

            System.out.print("Enter your password: ");
            password = sc.next();

            for (User user : users) {
                if (username.equals(user.getUsername())) {
                    validUsername = true;
                    userInDb = user;
                    break;
                }
            }
            if (userInDb != null && password.equals(userInDb.getPassword().getPasswordStr())) {
                validPassword = true;
            }
            if (validUsername && validPassword) {
                signedUser = userInDb;
                General.newPage();
                System.out.println("You are signed in.");
                break;
            }

            if (!validUsername) {
                System.out.println("User with this username is not signed up.");
                System.out.println("1. Sign up.");
                System.out.println("2. Try again.");
                System.out.println("3. <-Go Back.");
                int choice = sc.nextInt();
                if (choice == 1) {
                    General.newPage();
                    signUp();
                    break;
                } else if (choice == 2) {
                    General.newPage();
                    break;
                } else if (choice == 3) {
                    General.newPage();
                    break;
                } else General.wrongCommandMessage();
            }

            if (!validPassword) {
                System.out.println("Invalid password.");
                System.out.println("1. Sign up.");
                System.out.println("2. Try again.");
                System.out.println("3. <-Go Back.");
                int choice = sc.nextInt();
                if (choice == 1) {
                    General.newPage();
                    signUp();
                    break;
                } else if (choice == 2) {
                    General.newPage();
                } else if (choice == 3) {
                    General.newPage();
                    break;
                } else General.wrongCommandMessage();
            }
        }
    }

    private void signUp() {
        String name;
        String surname;
        String username;
        String passwordStr;
        Password password = new Password("");
        boolean existedUsername;

        while (true) {
            existedUsername = false;

            System.out.println("Enter your name: ");
            name = sc.next();

            System.out.println("Enter your surname: ");
            surname = sc.next();

            System.out.println("Enter your username: ");
            username = sc.next();

            for (User user : users) {
                if (username.equals(user.getUsername())) {
                    existedUsername = true;
                    System.out.println("This username is already taken.");
                    System.out.println("1. Try again.");
                    System.out.println("2. <-Go Back.");
                    int choice = sc.nextInt();
                    if (choice == 1) {
                        break;
                    } else if (choice == 2) {
                        General.newPage();
                        break;
                    } else General.wrongCommandMessage();
                }
            }
            if (existedUsername) break;

            while (true) {
                System.out.println("Enter your password: ");
                passwordStr = sc.next();
                if (!password.checkPassword(passwordStr)) {
                    General.invalidPasswordMessage();
                } else {
                    password.setPasswordStr(passwordStr);
                    break;
                }
            }

            System.out.println("Congratulations! You are joined to my application!");
            User newUser = new User(name, surname, username, password);
            addUser(newUser);
            General.newPage();
            break;
        }
    }

    private void addUser(User user) {
        users.add(user);
    }

    private void fillAdmins() throws FileNotFoundException {
        Scanner dbScanner = new Scanner(dbadmins);
        String club, name, surname, username;
        while (dbScanner.hasNext()) {
            club = dbScanner.next();
            name = dbScanner.next();
            surname = dbScanner.next();
            username = dbScanner.next();
            admins.add(new Admin(club, name, surname, username));
        }
    }

    private void saveAdminList() throws IOException {
        String data = "";
        for (Admin admin : admins) {
            data += admin + "\n";
        }
        Files.write(Paths.get(pathDBadmins),
                data.getBytes());
    }

    private boolean isAdmin(String searchAdmin) {

        for (Admin admin : admins) {
            if (admin.getUsername().equals(searchAdmin)) {
                return true;
            }
        }
        return false;
    }

    private void fillClubs() throws FileNotFoundException {
        Scanner dbScanner = new Scanner(dbclubs);
        String clubName, username;
        while (dbScanner.hasNext()) {
            clubName = dbScanner.next();
            username = dbScanner.next();
            clubs.add(new Club(clubName, username));
        }
    }

    private void saveClubList() throws IOException {
        String data = "";
        for (Club club : clubs) {
            data += club + "\n";
        }
        Files.write(Paths.get(pathDBclubs),
                data.getBytes());
    }

    private void logOff() {
        signedUser = null;
    }

    private void saveUserList() throws IOException {
        String data = "";
        for (User user : users) {
            data += user + "\n";
        }
        Files.write(Paths.get(dbPath),
                data.getBytes());
    }
}