package week3.domain;

/**
 * This is class Player
 * It is for players of Game
 * It contains playername and his score
 */

public class Player {

    private String playername;  //name of player
    private int score;          //player's score

    Player(User user){
        setPlayername(user.getUsername());
    }

    Player(String username, int score){
        setPlayername(username);
        setScore(score);
    }

    private void setPlayername(String playername) {
        this.playername = playername;
    }

    public String getPlayername() {
        return playername;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getScore() {
        return score;
    }

    @Override
    public String toString() {
        return playername+" "+score;
    }
}
