package week3.domain;

/**
 * This is class Password
 * Objects of this class store users' password
 * checkPassword(String password) checks, is password valid or not
 */

public class Password {

    private String passwordStr; //string of user's password

    Password(String password) {
        setPasswordStr(password);
    }

    public void setPasswordStr(String passwordStr) {
        this.passwordStr = passwordStr;
    }

    public String getPasswordStr() {
        return passwordStr;
    }

    public boolean checkPassword(String password) {
        if (password.length() > 9 || checkUpper(password)
            || checkLower(password) || checkDigit(password)) return true;
        return false;
    }

    private boolean checkUpper(String password) {
        for (int i = 0; i < password.length(); i++)
            if (Character.isUpperCase(password.charAt(i))) return true;
        return false;
    }

    private boolean checkLower(String password) {
        for (int i = 0; i < password.length(); i++)
            if (Character.isLowerCase(password.charAt(i))) return true;
        return false;
    }

    private boolean checkDigit(String password) {
        for (int i = 0; i < password.length(); i++)
            if (Character.isDigit(password.charAt(i))) return true;
        return false;
    }
}
